#pragma once

#include <iostream>
#include "User.h"
#include <vector>

using namespace std;

class User;

class Room
{

private:
	User * _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
	vector<User*> _users;

	void sendMessage(string msg);
	void sendMessage(string msg, User* excludeUser);

public:
	Room(User* _admin, int _maxUsers, int _questionTime, int _questionNo, string _name, int _id);
	~Room();

	int getQustionNo();
	int getId();
	string getName();
	string getUsersListMessage();

	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
};

