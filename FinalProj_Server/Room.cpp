#include "Room.h"


void Room::sendMessage(string msg)
{
	sendMessage(msg, nullptr);
}

void Room::sendMessage(string msg, User * excludeUser)
{
	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] == excludeUser || this->_users[i]->getUsername() == excludeUser->getUsername())
		{
			continue;
		}
		this->_users[i]->send(msg);
	}
}

Room::Room(User * admin, int maxUsers, int questionTime, int questionNo, string name, int id)
{
	this->_admin = admin;
	this->_id = id;
	this->_maxUsers = maxUsers;
	this->_name = name;
	this->_questionNo = questionNo;
	this->_questionTime = questionTime;
	this->_users.push_back(admin);
}

Room::~Room()
{
}

int Room::getQustionNo()
{
	return this->_questionNo;
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}

string Room::getUsersListMessage()
{
	string msg = "108";

	msg = msg + to_string(this->_users.size());

	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		int nameSize = this->_users[i]->getUsername().length();
		if (nameSize > 9)
			msg = msg + "##" + to_string(nameSize);
		else
			msg = msg + "##0" + to_string(nameSize);
		msg = msg + "##" + this->_users[i]->getUsername();
	}

	return msg;
}

bool Room::joinRoom(User * user)
{
	if (this->_maxUsers == this->_users.size())
	{
		user->send("1101");
		return false;
	}
	this->_users.push_back(user);
	this->_users[this->_users.size()]->send("1100 " + to_string(this->_questionNo) + " " + to_string(this->_questionTime));
	sendMessage(getUsersListMessage());
	return true;
}

void Room::leaveRoom(User * user)
{
	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] == user || this->_users[i]->getUsername() == user->getUsername())
		{
			this->_users.erase(this->_users.begin() + i);
			user->send("1120");
			sendMessage(getUsersListMessage());
		}
	}
}

int Room::closeRoom(User * user)
{
	if (user != this->_admin || user->getUsername() != this->_admin->getUsername())
		return -1;
	sendMessage("116");

	for (unsigned int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i] != user)
		{
			this->_users[i]->clearRoom();
		}
	}
	return this->_id;
}

