#pragma once

#include <iostream>
#include <Windows.h>
#include <string>
#include "Room.h"

using namespace std;

class Room;

class User
{
private:
	string _username;
	Room* _currRoom;
	SOCKET _sock;

public:
	User(string username, SOCKET sock);
	~User();

	void send(string data);

	string getUsername();
	SOCKET getSocket();
	Room* getRoom();

	bool createRoom(int roomId, string roomName, int maxUsers, int qustionNo, int questionTime);
	bool joinRoom(Room* room);

	void clearRoom();
	void leaveRoom();
	int closeRoom();
};

