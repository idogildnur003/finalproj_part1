#include "Validator.h"



bool Validator::isPasswordValid(string pass)
{
	bool digit = false, uCase = false, lCase = false;
	if (pass.length() < 4 || pass.find(' ') != string::npos || pass.find_first_of("0123456789") == std::string::npos || pass.find_first_of("abcdefghijklmnopqrstuvwxyz") == std::string::npos || pass.find_first_of("ABCDEFGHIJKLMNOPQRSTUVWXYZ") == std::string::npos)
		return false;
	return true;
}

bool Validator::isUsernameValid(string name)
{
	if (name.length() == 0 || name.find(' ') != string::npos || (!(name[0] < 'a' || name[0] > 'z') && !(name[0] < 'A' || name[0] > 'Z')))
	{
		return false;
	}
	return true;
}
