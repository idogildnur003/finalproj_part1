#include "TriviaServer.h"
#include <iostream>
#include <thread>

#define PORT 2250

#define BYTE_SIZE_P 2

#define P_EXIT 299
#define P_SIGN_OUT 201
#define P_SIGN_IN 200
#define P_SIGN_UP 203
#define P_LEAVE_GAME 222


TriviaServer::TriviaServer()
{
	_db = DataBase();
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}


TriviaServer::~TriviaServer()
{
	delete[] &_roomList;
	delete[] & _connectedUsers;
	try
	{
		::closesocket(_socket);
	}
	catch (...) 
	{
		throw std::exception(__FUNCTION__ " - socket");
	}
}

void TriviaServer::serve()
{
	bindAndListen();
	thread tr(&TriviaServer::handleRecievedMessages, this);
	tr.detach();

	while (true)
	{
		accept();
	}

}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	else if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");

	cout << "Listening on port " << PORT << endl;
}

void TriviaServer::accept()
{
	SOCKET client_socket = ::accept(_socket, NULL, NULL);
	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	thread handleClientThread(&TriviaServer::clientHandler, this, ref(client_socket));
	handleClientThread.detach();
}

void TriviaServer::clientHandler(SOCKET clientSock)
{
	int typeCode = -1;
	while (typeCode != 0 || typeCode != P_EXIT || typeCode != P_SIGN_OUT)
	{
		try
		{
			this->_queRcvMessages.push(buildRecieveMessage(clientSock, typeCode));
			typeCode = Helper::getMessageTypeCode(clientSock);
		}
		catch (...) {
			//this->_queRcvMessages.push(new RecievedMessage(clientSock, P_EXIT, "Connction Ended"));
		}
	}
}

User * TriviaServer::handleSignin(RecievedMessage * rcvMsg)
{
	string userName = rcvMsg->getValues()[0];
	string pass = rcvMsg->getValues()[1];
	if (this->_db.isUserAndPassMatch(userName, pass))
		if (getUserByName(userName) != nullptr)
		{
			User newUser = User(userName, this->_socket);
			newUser.send("1020");
			this->_connectedUsers.insert(pair<SOCKET, User*>(this->_socket, &newUser));
			return &newUser; //Talk to Avihi about returning an address; 
		}
		else
		{
			User currUser = *getUserByName(userName);
			currUser.send("1022");
			return &currUser; //Talk to Avihi about returning an address;
		}
	else
	{
		rcvMsg->getUser()->send("1021");
	}
}

bool TriviaServer::handleSignup(RecievedMessage * rcvMsg)
{
	string userName = rcvMsg->getValues()[0];
	string pass = rcvMsg->getValues()[1];
	string email = rcvMsg->getValues()[2];
	if(Validator::isPasswordValid(pass))
		if(Validator::isUsernameValid(userName))
			if (!this->_db.isUserExists(userName))
				if (this->_db.addNewUser(userName, pass, email))
				{
					//1040
					rcvMsg->getUser()->send("1040");
					return true;
				}
				else
				{
					//1044
					rcvMsg->getUser()->send("1044");
					return false;
				}
			else
			{
				//1042
				rcvMsg->getUser()->send("1042");
				return false;
			}
		else
		{
			//1043
			rcvMsg->getUser()->send("1043");
			return false;
		}
	else
	{
		//1041
		rcvMsg->getUser()->send("1041");
		return false;
	}
			
}

void TriviaServer::handleRecievedMessages()
{
	
}

//203(3), 209(1), 211, 213,  
RecievedMessage * TriviaServer::buildRecieveMessage(SOCKET sock, int num)
{
	vector<string> newVec;
	int numParams = 0;
	switch (num)
	{
		case P_SIGN_IN:
			numParams = 2;
			break;

		case P_SIGN_UP:
			numParams = 3;
			break;

		default:
			break;
	}

	for (int i = 0; i < numParams; i++)
	{
		int lenParam = Helper::getIntPartFromSocket(sock, BYTE_SIZE_P);
		string param = Helper::getStringPartFromSocket(sock, lenParam);
	}

	RecievedMessage rcvMsg = RecievedMessage(sock, num, newVec);
	return &rcvMsg; //Talk with Avihi about sending an address.
}


User * TriviaServer::getUserByName(string str)
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (it->second->getUsername() == str)
			return it->second;
	}
	return nullptr;
}

User * TriviaServer::getUserBySocket(SOCKET sock)
{
	map<SOCKET, User*>::iterator it;
	for (it = _connectedUsers.begin(); it != _connectedUsers.end(); it++)
	{
		if (it->first == sock)
			return it->second;
	}
	return nullptr;
}
