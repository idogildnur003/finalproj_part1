#pragma once

#include "Helper.h"
#include "DataBase.h"
#include "Room.h"
#include "User.h"
#include <Windows.h>
#include "Helper.h"
#include <mutex>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class RecievedMessage
{
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;

public:
	RecievedMessage(SOCKET sock, int pro);
	RecievedMessage(SOCKET sock, int pro, vector<string> strVec);
	~RecievedMessage();

	SOCKET getSock();
	User* getUser();
	void setUser(User* user);
	int getMessageCode();
	vector<string>& getValues();
};

