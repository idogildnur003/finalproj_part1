#include "User.h"

class Room;

User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
	this->_currRoom = nullptr;
}

User::~User()
{
}

void User::send(string data)
{

}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int qustionNo, int questionTime)
{
	if (this->_currRoom != nullptr)
	{
		this->send("1141");
		return false;
	}
	this->_currRoom = new Room(this, maxUsers, questionTime, qustionNo, roomName, roomId);
	this->send("1140");
	return true;
}

bool User::joinRoom(Room * room)
{
	if (this->_currRoom == nullptr)
	{
		return room->joinRoom(this);
	}
	return false;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

void User::leaveRoom()
{
	this->_currRoom->leaveRoom(this);
	this->_currRoom = nullptr;
}

int User::closeRoom()
{
	if (this->_currRoom == nullptr)
		return -1;
	int ret = this->_currRoom->closeRoom(this);
	if (ret == -1)
		return -1;
	this->_currRoom = nullptr;
	return ret;
}
