#pragma once

#include <map>
#include "Validator.h"

using namespace std;

class DataBase
{
private:
	map<string, pair<string, string>> _users;

public:
	DataBase();
	~DataBase();

	bool isUserExists(string name);
	bool addNewUser(string name, string pass, string email);
	bool isUserAndPassMatch(string name, string pass);
};

