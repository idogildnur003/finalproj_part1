#include "RecievedMessage.h"

RecievedMessage::RecievedMessage(SOCKET sock, int pro)
{
	this->_sock = sock;
	this->_messageCode = pro;
}

RecievedMessage::RecievedMessage(SOCKET sock, int pro, vector<string> strVec)
{
	this->_sock = sock;
	this->_messageCode = pro;
	this->_values = strVec;
}

RecievedMessage::~RecievedMessage()
{
	
}

SOCKET RecievedMessage::getSock()
{
	return this->_sock;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User * user)
{
	this->_user = user;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	// TODO: insert return statement here
	return this->_values;
}
