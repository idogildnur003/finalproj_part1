#pragma once

#include <iostream>

using namespace std;

class Validator
{
public:
	static bool isPasswordValid(string pass);
	static bool isUsernameValid(string name);

};

