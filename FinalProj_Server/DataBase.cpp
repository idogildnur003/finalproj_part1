#include "DataBase.h"


DataBase::DataBase()
{
}


DataBase::~DataBase()
{
}

bool DataBase::isUserExists(string name)
{
	if (_users.find(name) != _users.end())
		return true;
	return false;
}

bool DataBase::addNewUser(string name, string pass, string email)
{
	if (isUserExists(name) || !Validator::isPasswordValid(pass) || !Validator::isUsernameValid(name))
		return false;
	_users.insert(pair<string, pair<string, string>>(name, pair<string, string>(pass, email)));
	return true;
}

bool DataBase::isUserAndPassMatch(string name, string pass)
{
	if (_users.find(name)->second.first == pass)
	{
		return true;
	}
	return false;
}
