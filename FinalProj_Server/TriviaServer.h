#pragma once

#include "Helper.h"
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Room.h"
#include "User.h"
#include <Windows.h>
#include "Helper.h"
#include <mutex>
#include <fstream>
#include <queue>
#include <map>
#include <string>

using namespace std;


class TriviaServer
{
private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomList;

	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;

	int static _roomIdSequence;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET client);
	void safeDeleteUser(RecievedMessage* rcvMsg);

	User* handleSignin(RecievedMessage* rcvMsg);
	bool handleSignup(RecievedMessage* rcvMsg);
	void handleSignout(RecievedMessage* rcvMsg);

	void handleLeaveGame(RecievedMessage* rcvMsg);
	void handleStartGame(RecievedMessage* rcvMsg);
	void handlePlayerAnswer(RecievedMessage* rcvMsg);

	bool handleCreateRoom(RecievedMessage* rcvMsg);
	bool handleCloseRoom(RecievedMessage* rcvMsg);
	bool handleJoinRoom(RecievedMessage* rcvMsg);
	bool handleLeaveRoom(RecievedMessage* rcvMsg);
	void handleGetUserInRoom(RecievedMessage* rcvMsg);
	void handleGetRooms(RecievedMessage* rcvMsg);

	void handleGetBestScores(RecievedMessage* rcvMsg);
	void handleGetPersonalStatus(RecievedMessage* rcvMsg);

	void handleRecievedMessages();
	void addRecievedMessage(RecievedMessage* rcvMsg);
	RecievedMessage* buildRecieveMessage(SOCKET sock, int num);

	User* getUserByName(string str);
	User* getUserBySocket(SOCKET sock);
	Room* getRoomById(int id);

public:
	TriviaServer();
	~TriviaServer();

	void serve();

};

